export default () => ({
  isLoadingVulnerabilities: false,
  isLoadingVulnerabilitiesCount: false,
  pageInfo: {},
  vulnerabilities: [],
  vulnerabilitiesCount: {},
  errorLoadingVulnerabilities: false,
});
